
variable "APP_SERVER_PORT" {
  default = "8090"
}

variable "APP_SERVER_CONTAINER_PORT" {
  default = "8080"
}

variable "arquivoAmbiente" {
  default = ".env-dev"
}

variable "imagem" {
  default = ""
}
