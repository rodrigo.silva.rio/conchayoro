import jenkins.model.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.impl.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

domain = Domain.global()
store = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()

privateRepoKeys = ["key1", "key2"]

String keyfile = "/root/.ssh/id_rsa"
privateRepoKeys.each {
    privateKey = new BasicSSHUserPrivateKey(
        CredentialsScope.GLOBAL,
        "ssh-key",
        "jenkins",
        new BasicSSHUserPrivateKey.FileOnMasterPrivateKeySource(keyfile),
        "", 
        "Jenkins SSH Key"
    )   
    store.addCredentials(domain, privateKey)
}
