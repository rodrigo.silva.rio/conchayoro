resource "null_resource" "Deploy" {
  
  provisioner "local-exec" {
    command = "sh dev/excluirContainerLocal.sh"
  }
  provisioner "local-exec" {
    command = "docker run --rm -d --name containerLocal --env-file ${var.arquivoAmbiente} -p ${var.APP_SERVER_PORT}:${var.APP_SERVER_CONTAINER_PORT} ${var.imagem}"
  }
}