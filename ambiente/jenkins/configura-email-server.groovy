import jenkins.model.*

def inst = Jenkins.getInstance()

def desc = inst.getDescriptor("hudson.tasks.Mailer")

desc.setSmtpHost(System.getenv('SMTP_HOST_ADDRESS'))
desc.setSmtpPort(System.getenv('SMTP_HOST_PORT'))
desc.setSmtpAuth(System.getenv('SMTP_HOST_USER'), System.getenv('SMTP_HOST_PASSWORD'))
desc.setUseSsl(false)
desc.setCharset("UTF-8")
desc.setReplyToAddress(System.getenv('SMTP_HOST_REPLY_ADDRESS'))

desc.save()

