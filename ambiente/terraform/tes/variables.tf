variable "app_name" {
  default = "conchayoro"
}

variable "APP_SERVER_PORT" {
  default = "80"
}

variable "APP_SERVER_CONTAINER_PORT" {
  default = "8080"
}

variable "arquivoAmbiente" {
  default = ""
}

variable "imagem" {
  default = ""
}