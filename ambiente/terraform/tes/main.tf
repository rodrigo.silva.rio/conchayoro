provider "heroku" {
  version = "~> 2.0"
}

resource "heroku_build" "conchayoro_build" {
  app = var.app_name
  
  provisioner "local-exec" {    
    command = "docker login --username=${HEROKU_EMAIL} --password=${HEROKU_API_KEY} registry.heroku.com"
  }

  provisioner "local-exec" {    
    command = "docker push registry.heroku.com/${PROJECT_NAME}"
  }
}

output "example_app_url" {
  value = "https://${var.app_name}.herokuapp.com"
}